FROM openjdk:8-jdk-alpine
RUN wget -qO -  https://github.com/$(wget -qO - https://github.com/UniversalMediaServer/UniversalMediaServer/releases/latest | grep tgz | grep nofollow | tr '"' ' ' | awk '{print $3}' ) | tar -zxf -  && \
mv ums* ums && \
mkdir -p /root/.config/UMS/data && \
cp /ums/UMS.conf /root/.config/UMS
EXPOSE 5001
EXPOSE 9001
EXPOSE 1900
EXPOSE 1900/udp
CMD [ '/ums/UMS.sh' ]
